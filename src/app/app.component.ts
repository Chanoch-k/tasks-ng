import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import {  ConfirmationService,  Footer,  MenuItem,  MessageService,  SelectItem,} from 'primeng/api';
import { Observable, Subscription } from 'rxjs';
import { Welcome } from './models/messages/welcome';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
})
export class AppComponent implements OnInit {
  title = 'task';
  constructor(
    public messageService: MessageService,
    private confirmationService: ConfirmationService
  ) {}

  welComeMessage() {
    this.messageService.add(new Welcome());
  }

  ngOnInit() {}
}
