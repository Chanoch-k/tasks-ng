export class BlockLogin {
  severity = 'error';
  summary = 'אתה מחובר';
  detail =
    'הנך מחובר כבר למערכת. אם ברצונך להתחבר כמשתמש אחר עליך להתנתק תחילה.';
  life = 3000;
}
