export class AddUsersuccess {
  severity = 'success';
  summary = 'אישור';
  detail = 'משתמש נוסף בהצלחה';
  life = 3000;
}
export class AddUserSrvError {
  severity = 'error';
  summary = 'שגיאה';
  detail = 'שגיאת שרת';
  life = 3000;
}

export class AddUserError {
  severity = 'error';
  summary = 'שגיאה';
  detail = 'שגיאה לא ידועה אנא פנה למנהל המערכת ';
  life = 3000;
}
