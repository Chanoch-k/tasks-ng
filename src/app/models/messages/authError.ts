
export class AuthError {
  severity = 'error';
  summary = 'שגיאת התחברות';
  detail = 'המייל או הסיסמה  שהוקלדו שגויים, אנא נסה שנית';
  life = 3000;

}
