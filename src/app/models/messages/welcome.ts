export class Welcome {
  severity = 'info';
  summary = 'ברוך הבא!';
  detail = 'להתחברות למערכת הקלד אימייל וסיסמה';
  life = 2300;
}
