
export class ErrorMsg {
  severity = 'warn';
  summary = 'תקלה';
  detail = ' שגיאה לא ידועה אנא פנה למנהל המערכת';
  life = 3000;
}
export class SrvErrorMsg {
  severity = 'error';
  summary = 'שגיאה';
  detail = 'שגיאת שרת';
  life = 3000;
}
/*task*/
export class AddTasksuccess {
  severity = 'success';
  summary = 'אישור';
  detail = 'משימה נוספה בהצלחה';
  life = 3000;
}
export class AddLabelsError {
  severity = 'warn';
  summary = 'תקלה';
  detail = 'תקלה בהוספת התוויות';
  life = 3000;
}
export class EditTaskSuccess {
  severity = 'success';
  summary = 'אישור';
  detail = 'משימה עודכנה בהצלחה';
  life = 3000;
}
export class DeleteTaskSuccess {
  severity = 'success';
  summary = 'אישור';
  detail = 'משימה נמחקה';
  life = 3000;
}
/*label*/
export class AddLabelSuccess {
  severity = 'success';
  summary = 'אישור';
  detail = 'התווית נוספה בהצלחה';
  life = 3000;
}
export class EditLabelSuccess {
  severity = 'success';
  summary = 'אישור';
  detail = 'התווית עודכנה בהצלחה';
  life = 3000;
}
export class DeleteLabelSuccess {
  severity = 'success';
  summary = 'אישור';
  detail = 'התווית נמחקה';
  life = 3000;
}
/**/
export class AddSuccessMsg {
  severity = 'success';
  summary = 'אישור';
  detail = ' נוסף בהצלחה';
  life = 3000;
}


