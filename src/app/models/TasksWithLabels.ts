export class TasksWithLabels {
  id!: number;
  name!: string;
  description!: string;
  due_date!: Date;
  completed!: boolean;
  user_id!: number;
  prod_datedate!: Date;
  //projectId!: number;
  // ownerId!: number;
  labels:number[]=[];
}
