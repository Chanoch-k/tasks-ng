export class SideBarButton {
  icon!: string;
  text!: string;
  navigate!: string;
}
