export class InfoCard {
  icon!: string | null;
  text!: string | null;
  data!: number |string| null;
  color!: string | null;
  textColor!: string | null| undefined;
}
