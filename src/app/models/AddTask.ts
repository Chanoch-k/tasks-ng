export class AddTask {
  name!: string | null;
  description!: string | null;
  due_date!: Date | null;
  completed!: boolean | null;
  user_id!: number | null;
  prod_date!: Date | null;
  labels!: number[] | null;
  //projectId!: number;
  // ownerId!: number;
}
