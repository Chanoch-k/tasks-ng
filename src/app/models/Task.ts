export class Task {
  id: number = -1;
  name: string = '';
  description: string = '';
  due_date: Date = new Date();
  completed: boolean | null = false;
  user_id: number = 1;
  user_name: string = '';
  prod_date: Date = new Date();
  //projectId!: number;
  // ownerId!: number;
  labels: number[] = [];
}


