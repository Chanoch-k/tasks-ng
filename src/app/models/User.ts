export class User {
    id!: number;
    phone!: string;
    password!: string;
    email!: string;
    name!: string;
    last_name!: string;
    role!: number;
    group_id!:number;
}
