export class LoginResponse {
  bearerString:string|null = null;
  roll:number|null = null;
  name:string|null = null;
}
