import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { Scn404Component } from './screens/errors/scn404/scn404.component';
import { LoginComponent } from './screens/login/login.component';
import { TasksScComponent } from './screens/tasks-sc/tasks-sc.component';
import { TestComponent } from './components/test/test.component';
import { AddTaskComponent } from './components/add-task/add-task.component';
import { TasksComponent } from './components/tasks/tasks.component';
import { AddUserComponent } from './components/add-user/add-user.component';
import { UsersScComponent } from './screens/users-sc/users-sc.component';
import { UserTimesChartComponent } from './components/widgets/user-times-chart/user-times-chart.component';
import { TimelineScComponent } from './screens/timeline-sc/timeline-sc.component';
import { LabelsScComponent } from './screens/labels-sc/labels-sc.component';
import { Test2Component } from './components/test2/test2.component';

/*temp*/

const routes: Routes = [
  { path: 'login', component: LoginComponent },
  { path: '', redirectTo: 'login', pathMatch: 'full' }, //login
  { path: 'app', component: TasksScComponent },
  { path: 'add-task', component: AddTaskComponent },
  { path: 'task', component: TasksComponent },
  { path: 'labels', component: LabelsScComponent },
  { path: 'users', component: UsersScComponent },
  { path: 'add-user', component: AddUserComponent },
  { path: 'time-line', component: TimelineScComponent },

  // { path: 'tasks', component: TaskTableComponent },

  { path: 'test', component: TestComponent },
  { path: 'test5', component: TestComponent },
  { path: 'test2', component: Test2Component },
  { path: '**', component: Scn404Component },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
