import {  Component,  Input,  OnInit,} from '@angular/core';
import { Task } from 'src/app/models/Task';
import { LabelService } from 'src/app/services/label.service';
import { LabelList } from 'src/app/models/LabelList';

@Component({
  selector: 'app-timeline',
  templateUrl: './timeline.component.html',
  styleUrls: ['./timeline.component.css'],
})
export class TimelineComponent implements OnInit {
  labelList: LabelList[] = [];
  @Input() userTasks: Task[] = [];
  events1: any[] = [];
  currentDate: Date = new Date();
  constructor(
    private labelService: LabelService,
  ) {

 this.currentDate = new Date('07/12/2023');
  }

  ngOnInit(): void {

    this.labelService
      .GetLabelList()
      .subscribe((data) => (this.labelList = data));
  }

}
