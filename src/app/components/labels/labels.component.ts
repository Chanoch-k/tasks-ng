import { Component, OnInit } from '@angular/core';
import { LabelList } from 'src/app/models/LabelList';
import { MessageService } from 'primeng/api';
import { LabelService } from 'src/app/services/label.service';
import { Respon } from 'src/app/models/Respon';
import { ConfirmationService } from 'primeng/api';
import { ConfirmDialogModule } from 'primeng/confirmdialog';

import {
  AddLabelSuccess,
  EditLabelSuccess,
  DeleteLabelSuccess,
  ErrorMsg,
  SrvErrorMsg,
} from '../../models/messages/messages';
@Component({
  selector: 'app-labels',
  templateUrl: './labels.component.html',
  styleUrls: ['./labels.component.css'],
})
export class LabelsComponent implements OnInit {
  labels: LabelList[] = [];
  respon!: Respon;
  editMode: number = 0;
  editLabel!: LabelList;
  addMode: boolean = false;
  addLabel: LabelList = new LabelList();

  constructor(
    private labelService: LabelService,
    private messageService: MessageService,
    private confirmationService: ConfirmationService
  ) {
    /* this.labels = [
      { id: 0, name: 'aaaaa', color: '', parent_id: 0 },
      { id: 1, name: 'bbbbb', color: '', parent_id: 0 },
    ];*/
    /*
    this.addLabel = { id: 0, name: '', parent_id: 0, color: '#ffffff' };
    this.editLabel = { id: 0, name: 'abc', parent_id: 0, color: '#d63384' };*/
  }
  delete(id: number) {
    this.confirmationService.confirm({
      message: 'האם אתה בטוח שברצונך למחוק תווית זו? ',
      header: 'מחיקת תווית ',
      icon: 'pi pi-trash',
      acceptLabel: 'מחק',
      rejectLabel: 'ביטול',
      acceptButtonStyleClass:
        'p-button-raised p-button-sm p-button-danger p-button-text',
      rejectButtonStyleClass:
        'p-button-raised p-button-sm p-button-text p-button-plain',
      acceptIcon: 'pi',
      rejectIcon: 'pi',
      accept: () => {
        this.labelService.DLabel(id).subscribe((data) => {
          this.respon = data;
        });
      },
      reject: () => {},
    });
  }

  edit(label: LabelList) {
    this.editMode = label.id;
    this.editLabel = {
      id: label.id,
      name: label.name,
      parent_id: label.parent_id,
      color: label.color,
    };
  }
  cancel() {
    this.editMode = 0;
    this.addMode = false;
    this.editLabel = new LabelList();
    this.addLabel = new LabelList();
  }
  update() {
    this.labelService.ULabel(this.editLabel).subscribe((data) => {
      this.respon = data;
      if (this.respon.status == 200) {
        this.messageService.add(new EditLabelSuccess());
      } else if (this.respon.status == 500) {
        this.messageService.add(new SrvErrorMsg());
      } else {
        this.messageService.add(new ErrorMsg());
      }
    });
    this.editLabel = new LabelList();
    this.editMode = 0;
  }
  add() {
    this.labelService.ALabel(this.addLabel).subscribe((data) => {
      this.respon = data;
      if (this.respon.status == 200) {
        this.messageService.add(new AddLabelSuccess());
      } else if (this.respon.status == 500) {
        this.messageService.add(new SrvErrorMsg());
      } else {
        this.messageService.add(new ErrorMsg());
      }
    });
    this.addLabel = new LabelList();
    this.addMode = false;
  }
  ngOnInit(): void {
    this.labelService.labels$.subscribe((labels) => (this.labels = labels));
    this.labelService.refreshLabels();
  }
}
