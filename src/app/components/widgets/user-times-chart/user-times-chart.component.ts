import { Component, Input, OnInit, ViewChild } from '@angular/core';
import { TaskService } from 'src/app/services/task.service';
import { Task } from 'src/app/models/Task';

@Component({
  selector: 'app-user-times-chart',
  templateUrl: './user-times-chart.component.html',
  styleUrls: ['./user-times-chart.component.css'],
})
export class UserTimesChartComponent implements OnInit {
  tasks: Task[] = [];
  data: any;
  @Input() id: number = 0;
  @Input() userTasks: Task[] = [];

  //userTasks!: Task[];
  chartOptions: any;
  early: number = 0;
  onTime: number = 0;
  late: number = 0;
  temp!: number;
  constructor(private taskService: TaskService) {}

  ngOnInit(): void {
    /*this.taskService.GetTasks().subscribe((data) => (this.tasks = data));
      this.userTasks = this.tasks.filter((task) => task.user_id === this.id);*/


    this.Sort(this.userTasks);

    this.data = {
      labels: ['מוקדם', 'בזמן', 'מאוחר'],
      datasets: [
        {
          data: [this.early, this.onTime, this.late],
          backgroundColor: ['#42A5F5', '#66BB6A', '#FFA726'],
          hoverBackgroundColor: ['#64B5F6', '#81C784', '#FFB74D'],
        },
      ],
    };
  }

  Sort(tasks: Task[]) {
    tasks.forEach((task) => {
      /*   const timeDifference = task.prod_date.getTime() - task.due_date.getTime();
      const daysDifference = timeDifference / (1000 * 3600 * 24);
      console.log('temp= ' + daysDifference);*/
      if (task.completed == true) {
        if (task.due_date > task.prod_date) {
          this.late++;
        } else if (task.due_date < task.prod_date) {
          this.early++;
        } else {
          this.onTime++;
        }
      }
    });
  }
}
