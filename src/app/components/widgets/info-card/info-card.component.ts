import { Component, OnInit } from '@angular/core';
import { InfoCard } from 'src/app/models/InfoCard';
import { TaskService } from 'src/app/services/task.service';
import { Task } from 'src/app/models/Task';

@Component({
  selector: 'app-info-card',
  templateUrl: './info-card.component.html',
  styleUrls: ['./info-card.component.css'],
})
export class InfoCardComponent implements OnInit {
  infoCard: InfoCard[] = [];
  tasks: Task[] = [];
  allTasks!: number | null;
  doneTasks!: number | null;
  pendingTasks!: number | null;

  constructor(private taskSvc: TaskService) {
    this.infoCard = [
      {
        icon: 'pi-list',
        text: 'משימות',
        data: this.allTasks,
        color: '#E2F1FF',
        textColor: null,
      },
      {
        icon: 'pi-check-square',
        text: 'בוצעו',
        data: this.doneTasks,
        color: '#DCF7EE',
        textColor: null,
      },
      {
        icon: 'pi-clock',
        text: 'ממתינים',
        data: 7,
        color: '#FFEFDF',
        textColor: null,
      },
      {
        icon: 'pi-exclamation-circle',
        text: '????/',
        data: 3,
        color: '#FAE9F4',
        textColor: null,
      },
    ];


  }

  ngOnInit(): void {
 this.taskSvc.GetTasks().subscribe((data) => (this.tasks = data));
 this.allTasks = this.tasks.length;
 this.doneTasks = this.tasks.filter((t) => t.completed == true).length;
  }
}
