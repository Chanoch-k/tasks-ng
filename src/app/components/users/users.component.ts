import { Component, OnInit, EventEmitter, Output } from '@angular/core';
import { User } from 'src/app/models/User';
import { UsersService } from 'src/app/services/users.service';
import { TaskService } from 'src/app/services/task.service';
import { Task } from 'src/app/models/Task';
import { from } from 'rxjs';

@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.css'],
})
export class UsersComponent implements OnInit {
  users!: User[];
  user: User | undefined = undefined;

  //UserTimesChart
  tasks: Task[] = [];
  userTasks: Task[] = [];
  data: any;
  //userId: number = 0;
  chartOptions: any;
  early: number = 0;
  onTime: number = 0;
  late: number = 0;
  temp!: number;
  constructor(
    private usersService: UsersService,
    private taskService: TaskService,
  ) {
    this.usersService.users$.subscribe((users) => (this.users = users));
    this.usersService.refreshUsers();

    this.taskService.tasks$.subscribe((tasks) => {
      this.tasks = tasks;
    });
    this.taskService.refreshTasks();
  }

  isAdmin(role: number) {
    if (role < 5) return true;
    else return false;
  }
  showUser(id: number) {
    this.userTasks = this.tasks.filter((t) => t.user_id == id);
    this.user = this.users.find((i) => i.id === id);
    this.Sort(this.userTasks);
    this.data = {
      labels: ['מוקדם', 'בזמן', 'מאוחר'],
      datasets: [
        {
          data: [this.early, this.onTime, this.late],
          backgroundColor: ['#42A5F5', '#66BB6A', '#FFA726'],
          hoverBackgroundColor: ['#64B5F6', '#81C784', '#FFB74D'],
        },
      ],
    };
  }
  ngOnInit(): void {
    // this.usersService.GetUsers().subscribe((data) => (this.users = data));
  }

  //UserTimesChart
  Sort(tasks: Task[]) {
    this.early = 0;
    this.onTime = 0;
    this.late = 0;
    tasks.forEach((task) => {
      /*   const timeDifference = task.prod_date.getTime() - task.due_date.getTime();
      const daysDifference = timeDifference / (1000 * 3600 * 24);
      console.log('temp= ' + daysDifference);*/
      if (task.completed == true) {
        if (task.due_date > task.prod_date) {
          this.late++;
        } else if (task.due_date < task.prod_date) {
          this.early++;
        } else {
          this.onTime++;
        }
      }
    });
  }
}
