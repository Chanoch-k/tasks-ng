import { Component, OnInit } from '@angular/core';
import { User  } from 'src/app/models/User';
import { AddUser } from 'src/app/models/AddUser';
import { UsersService } from 'src/app/services/users.service';
import { MessageService } from 'primeng/api';
import { Respon } from 'src/app/models/Respon';
import {
  AddUserError,
  AddUsersuccess,
  AddUserSrvError,
} from '../../models/messages/addUserMsg';
import { Router } from '@angular/router';

@Component({
  selector: 'app-add-user',
  templateUrl: './add-user.component.html',
  styleUrls: ['./add-user.component.css'],
})
export class AddUserComponent implements OnInit {
  user: User = new User();

  respon!: Respon;
  constructor(
    private usersService: UsersService,
    private messageService: MessageService,
      private router: Router,
  ) {}
  onSubmit() {
    this.usersService.AUser(this.user).subscribe((data) => {
      this.respon = data;
      if (this.respon.status == 200) {
        this.messageService.add(new AddUsersuccess());
         this.router.navigate(['/users']);
      } else if (this.respon.status == 500) {
        this.messageService.add(new AddUserError());
      } else {
        this.messageService.add(new AddUserSrvError());
      }
    });
  }
  ngOnInit(): void {}
}
