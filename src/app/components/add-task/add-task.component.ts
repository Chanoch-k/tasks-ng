import {
  Component,
  OnInit,
  Input,
  HostListener,
  Output,
  EventEmitter,
  ɵsetAllowDuplicateNgModuleIdsForTest,
} from '@angular/core';
import { LabelList } from 'src/app/models/LabelList';
import { InputTextModule } from 'primeng/inputtext';
import { Task } from 'src/app/models/Task';
import { AddTask } from 'src/app/models/AddTask';
import { TaskService } from 'src/app/services/task.service';
import { LabelService } from 'src/app/services/label.service';
import { MessageService } from 'primeng/api';
import {
  AddTasksuccess,
  AddLabelsError,
  EditTaskSuccess,
  ErrorMsg,
  SrvErrorMsg,
} from '../../models/messages/messages';
import { Respon } from 'src/app/models/Respon';
import { Observable, Subject } from 'rxjs';

@Component({
  selector: 'app-add-task',
  templateUrl: './add-task.component.html',
  styleUrls: ['./add-task.component.css'],
})
export class AddTaskComponent implements OnInit {
  task: Task = new Task();
  @Input() editTask: Task = new Task();
  labelList: LabelList[] = [];
  selectedLabels: LabelList[] = [];
  respon!: Respon;
  bool: boolean = false;
  bool2: boolean = false;
  constructor(
    private taskService: TaskService,
    private labelService: LabelService,
    private messageService: MessageService
  ) {

       this.labelService.labels$.subscribe((labels) => {
         this.labelList = labels;
       });
  }

  onSubmit() {
    if (this.editTask.id < 0) this.AddTask();
    else this.EditTask();
  }
  AddTask() {
    this.task.prod_date = new Date(Date.now());
    this.task.labels = this.selectedLabels.map((item) => item.id);
    this.taskService.ATask(this.task).subscribe((data) => {
      this.respon = data;
      if (this.respon.status == 200) {
        this.messageService.add(new AddTasksuccess());
      } else if (this.respon.status == 206) {
        this.messageService.add(new AddLabelsError());
      } else if (this.respon.status == 500) {
        this.messageService.add(new SrvErrorMsg());
      } else {
        this.messageService.add(new ErrorMsg());
      }

      this.close();
    });
  }
  EditTask() {
    this.task.prod_date = new Date(Date.now());
    this.task.due_date=this.editTask.due_date;
    this.task.labels = this.selectedLabels.map((item) => item.id);
    this.taskService.UTask(this.task).subscribe((data) => {
      this.respon = data;
      if (this.respon.status == 200) {
        this.messageService.add(new EditTaskSuccess());
      } else if (this.respon.status == 500) {
        this.messageService.add(new SrvErrorMsg());
      } else {
        this.messageService.add(new ErrorMsg());
      }
      this.close();
    });
  }
  AddLabel(label: LabelList) {
    this.selectedLabels.push(label);
    const index = this.labelList.indexOf(label);
    if (index > -1) this.labelList.splice(index, 1);
  }
  RemoveLabel(label: LabelList) {
    this.labelList.push(label);
    const index = this.selectedLabels.indexOf(label);
    if (index > -1) this.selectedLabels.splice(index, 1);
  }

  ngOnInit(): void {

     if (this.editTask.id > 0) {
       this.task = this.editTask;
       /*not work */
       for (let i = 0; i < this.editTask.labels.length; i++) {
         const label = this.labelList.find(
           (l) => l.id == this.editTask.labels[i]
         );
         if (label != null){ this.AddLabel(label)};
       }
     }
    this.labelService.refreshLabels();
    this.taskService.refreshTasks();
  }

  @Output() closePopUp = new EventEmitter<boolean>();
  close() {
    this.editTask= new Task();
    this.closePopUp.emit(false);
  }
}
