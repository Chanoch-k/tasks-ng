import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { RouterModule } from '@angular/router';
import { SideBarButton } from 'src/app/models/SideBarButton';


@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.css'],
})
export class SidebarComponent implements OnInit {
  sideBarButton: SideBarButton[] = [];
  constructor(private router: Router) {
    this.sideBarButton = [
      //  {icon:'bi bi-grid-1x2',text:'דאשבורד',navigate:'/'},//bi bi-grid-1x2-fill
      //  {icon:'bi bi-people',text:'משתמשים',navigate:'/'},
      { icon: 'bi bi-ui-checks', text: 'משימות', navigate: '/app' },
      { icon: 'bi bi-plus-lg', text: 'יצירה', navigate: '/add-task' },
      { icon: 'bi bi-sliders', text: 'לוח זמנים', navigate: '/time-line' },
      { icon: 'bi bi-graph-up', text: 'סטיסטיקות', navigate: '/' },
      { icon: 'bi bi-tags', text: 'תוויות', navigate: '/labels' },
      { icon: 'bi bi-people-fill', text: 'משתמשים', navigate: '/users' },
      { icon: 'bi bi-gear', text: 'הגדרות', navigate: '/' },
    ];
  }
  navigate(str: string) {
    if (str == 'add') {
  this.router.navigate([str]);//temp

    } else {
      this.router.navigate([str]);
    }
  }
  ngOnInit(): void {}
}

