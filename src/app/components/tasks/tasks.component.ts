import {
  Component,
  OnInit,
  HostListener,
  Input,
  Output,
  EventEmitter,
} from '@angular/core';
import { Task } from 'src/app/models/Task';
import { TasksWithLabels } from 'src/app/models/TasksWithLabels';
import { User } from 'src/app/models/User';
import { Labels } from 'src/app/models/Labels';
import { TaskService } from 'src/app/services/task.service';
import { SaerchTasks } from 'src/app/models/SaerchTasks';
import { LabelService } from 'src/app/services/label.service';
import { LabelList } from 'src/app/models/LabelList';
import { Respon } from 'src/app/models/Respon';
import {ConfirmationService} from 'primeng/api';
import { InfoCard } from 'src/app/models/InfoCard';

import { AddTask } from 'src/app/models/AddTask';
import {
  EditTaskSuccess,
  DeleteTaskSuccess,
  ErrorMsg,
  SrvErrorMsg,
} from '../../models/messages/messages';
import { MessageService } from 'primeng/api';

@Component({
  selector: 'app-tasks',
  templateUrl: './tasks.component.html',
  styleUrls: ['./tasks.component.css'],
})
export class TasksComponent implements OnInit {
  infoCard: InfoCard[] = [];
  tasks: Task[] = [];
  labelList: LabelList[] = [];
  respon!: Respon;
  loading: boolean = true;
  rowsPerPageOptions = [10, 25, 50];
  first = 0;
  rows = this.rowsPerPageOptions[0];
  constructor(
    private taskSvc: TaskService,
    private labelService: LabelService,
    private confirmationService: ConfirmationService,
    private messageService: MessageService
  ) {
    //this.taskSvc.tasks$.subscribe((tasks) => (this.products = tasks));
    //  this.taskSvc.refreshTasks();
     this.infoCard = [
       {
         icon: 'pi-list',
         text: 'בטיפול',
         data: 5,
         color: '#FFFBEB',
         textColor: '#b45309',
       },
       {
         icon: 'pi-check-square',
         text: 'בוצעו',
         data: 2,
         color: '#ecfdf5',
         textColor: '#047857',
       },
       {
         icon: 'pi-clock',
         text: 'ממתינים',
         data: 0,
         color: '#fdf2f8',
         textColor: '#be185d',
       },
       {
         icon: 'bi bi-hourglass',
         text: 'קרוב ',
         data: '16:30',
         color: '#f0f9ff',
         textColor: '#096ca3',
       },
     ];
  }
  delete(event: Event, id: number) {
    this.confirmationService.confirm({
      target: event.target ?? undefined,
      header: 'מחיקת משימה',
      message: 'האם אתה בטוח שברצונך למחוק משימה זו?',
      icon: 'bi bi-trash3-fill',
      acceptLabel: 'מחיקה',
      rejectLabel: 'ביטול',
      defaultFocus: 'none',
      acceptButtonStyleClass: 'p-button-outlined p-button-danger p-button-text',
      rejectButtonStyleClass:
        'p-button-outlined p-button-secondary  p-button-text',
      accept: () => {
        this.taskSvc.DTask(id).subscribe((data) => {
          this.respon = data;
          if (this.respon.status == 200) {
            this.messageService.add(new DeleteTaskSuccess());
          } else if (this.respon.status == 500) {
            this.messageService.add(new SrvErrorMsg());
          } else {
            this.messageService.add(new ErrorMsg());
          }
        });
      },
      reject: () => {},
    });
  }
  @Output() editTask = new EventEmitter<Task>();
  edit(task: Task) {
    this.editTask.emit(task);
  }

  ngOnInit(): void {
    this.taskSvc.tasks$.subscribe((tasks) => {
      this.tasks = tasks;
      this.loading = false;
    });
    this.taskSvc.refreshTasks();

    this.labelService.labels$.subscribe((labels) => (this.labelList = labels));
    this.labelService.refreshLabels();
    /*  this.labelService
      .GetLabelList()
      .subscribe((date) => (this.labelList = date));*/
  }
  //page in table
  next() {
    this.first = this.first + this.rows;
  }
  prev() {
    this.first = this.first - this.rows;
  }
  reset() {
    this.first = 0;
  }
  isLastPage(): boolean {
    return this.tasks ? this.first === this.tasks.length - this.rows : true;
  }

  isFirstPage(): boolean {
    return this.tasks ? this.first === 0 : true;
  }
  onPageChange(event: any) {
    this.first = event.first;
    this.rows = event.rows;
  }
  currentPage() {
    return Math.round(this.tasks.length / this.rows).toString();
  }
}

