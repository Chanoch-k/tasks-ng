import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule } from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { DatePipe } from '@angular/common';
import { ActivatedRoute } from '@angular/router';
import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { ReactiveFormsModule } from '@angular/forms';
import { HttpErrorInterceptorService } from './services/http-error-interceptor.service';

//primeng
import { HttpClientModule } from '@angular/common/http';
import { ConfirmationService, MessageService } from 'primeng/api';
import { ToolbarModule } from 'primeng/toolbar';
import { TableModule } from 'primeng/table';
import { ToastModule } from 'primeng/toast';
import { CalendarModule } from 'primeng/calendar';
import { SliderModule } from 'primeng/slider';
import { MultiSelectModule } from 'primeng/multiselect';
import { ContextMenuModule } from 'primeng/contextmenu';
import { DialogModule } from 'primeng/dialog';
import { ButtonModule } from 'primeng/button';
import { DropdownModule } from 'primeng/dropdown';
import { ProgressBarModule } from 'primeng/progressbar';
import { InputTextModule } from 'primeng/inputtext';
import { InputSwitchModule } from 'primeng/inputswitch';
import { CheckboxModule } from 'primeng/checkbox';
import { TooltipModule } from 'primeng/tooltip';
import { ChartModule } from 'primeng/chart';
import { AvatarModule } from 'primeng/avatar';
import { DynamicDialogModule, DialogService ,DynamicDialogRef} from 'primeng/dynamicdialog';

import { DragDropModule } from 'primeng/dragdrop';
import { AvatarGroupModule } from 'primeng/avatargroup';
import { TagModule } from 'primeng/tag';
import { InputNumberModule } from 'primeng/inputnumber';
import { CardModule } from 'primeng/card';
import { PasswordModule } from 'primeng/password';
import { ProgressSpinnerModule } from 'primeng/progressspinner';
import { SplitButtonModule } from 'primeng/splitbutton';
import { FieldsetModule } from 'primeng/fieldset';
import { TimelineModule } from 'primeng/timeline';
import { BadgeModule } from 'primeng/badge';
import { ConfirmPopupModule } from 'primeng/confirmpopup';
import { ConfirmDialogModule } from 'primeng/confirmdialog';
import { ChipModule } from 'primeng/chip';
import { InputTextareaModule } from 'primeng/inputtextarea';
import { DividerModule } from 'primeng/divider';
import { ColorPickerModule } from 'primeng/colorpicker';
import { PaginatorModule } from 'primeng/paginator';
import { MenubarModule } from 'primeng/menubar';
import { MegaMenuModule } from 'primeng/megamenu';
import { KnobModule } from 'primeng/knob';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { SidebarComponent } from './components/sidebar/sidebar.component';
import { LoginComponent } from './screens/login/login.component';
import { Scn403Component } from './screens/errors/scn403/scn403.component';
import { Scn404Component } from './screens/errors/scn404/scn404.component';
import { HomeComponent } from './screens/home/home.component';
import { TestComponent } from './components/test/test.component';
import { InfoCardComponent } from './components/widgets/info-card/info-card.component';
import { TimelineComponent } from './components/timeline/timeline.component';
import { TasksScComponent } from './screens/tasks-sc/tasks-sc.component';
import { NavbarComponent } from './components/navbar/navbar.component';
import { TasksComponent } from './components/tasks/tasks.component';
import { AddTaskComponent } from './components/add-task/add-task.component';
import { AddUserComponent } from './components/add-user/add-user.component';
import { UsersComponent } from './components/users/users.component';
import { UsersScComponent } from './screens/users-sc/users-sc.component';
import { UserTimesChartComponent } from './components/widgets/user-times-chart/user-times-chart.component';
import { TimelineScComponent } from './screens/timeline-sc/timeline-sc.component';
import { LabelsComponent } from './components/labels/labels.component';
import { LabelsScComponent } from './screens/labels-sc/labels-sc.component';
import { Test2Component } from './components/test2/test2.component';

@NgModule({
  declarations: [
    AppComponent,
    SidebarComponent,
    LoginComponent,
    Scn403Component,
    Scn404Component,
    HomeComponent,
    TestComponent,
    InfoCardComponent,
    TimelineComponent,
    TasksScComponent,
    NavbarComponent,
    TasksComponent,
    AddTaskComponent,
    AddUserComponent,
    UsersComponent,
    UsersScComponent,
    UserTimesChartComponent,
    TimelineScComponent,
    LabelsComponent,
    LabelsScComponent,
    Test2Component,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    TableModule,
    HttpClientModule,
    ToastModule,
    BrowserAnimationsModule,
    ReactiveFormsModule,
    InputTextModule,
    CalendarModule,
    MultiSelectModule,
    BadgeModule,
    ChipModule,
    ButtonModule,
    AvatarModule,
    TagModule,
    ConfirmPopupModule,
    ConfirmDialogModule,
    DynamicDialogModule,
    DialogModule,
    InputTextareaModule,
    SplitButtonModule,
    CheckboxModule,
    DividerModule,
    ChartModule,
    TimelineModule,
    TooltipModule,
    DragDropModule,
    ColorPickerModule,
    PaginatorModule,
    DropdownModule,
    MenubarModule,
    MegaMenuModule,
    KnobModule,

    /*
     CardModule,
    ToolbarModule,
    ,
    ,
    ,
    SliderModule,
    ,
    ,
    ContextMenuModule,
    DropdownModule,
    ,
    ToastModule,
    ProgressBarModule,
    ToolbarModule,
    SliderModule,
    InputSwitchModule,
    AvatarGroupModule,
    TagModule,
    InputNumberModule,
    ,
    PasswordModule,
    ProgressSpinnerModule,
    SplitButtonModule,
    FieldsetModule,
    ,

    ,*/
  ],
  providers: [
    MessageService,
    DatePipe,
    ConfirmationService,
    DialogService,

    {
      provide: HTTP_INTERCEPTORS,
      useClass: HttpErrorInterceptorService,
      multi: true,
    },
  ],
  bootstrap: [AppComponent],
})
export class AppModule {}
