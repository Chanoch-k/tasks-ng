import { ComponentFixture, TestBed } from '@angular/core/testing';

import { Scn403Component } from './scn403.component';

describe('Scn403Component', () => {
  let component: Scn403Component;
  let fixture: ComponentFixture<Scn403Component>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ Scn403Component ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(Scn403Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
