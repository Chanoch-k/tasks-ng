import { Component, OnInit } from '@angular/core';
import { AuthService } from 'src/app/services/auth.service';
import { TaskService } from 'src/app/services/task.service';
import { Task } from 'src/app/models/Task';

@Component({
  selector: 'app-timeline-sc',
  templateUrl: './timeline-sc.component.html',
  styleUrls: ['./timeline-sc.component.css'],
})
export class TimelineScComponent implements OnInit {
  tokenClaims: any = { id: 0, role: 0 };
  userId: number = 0;
  tasks: Task[] = [];
  userT: Task[] = [];
  constructor(
    private authService: AuthService,
    private taskService: TaskService
  ) {
    this.authService.GetTokenClaims().subscribe((data) => {
      this.tokenClaims = data;
      this.userId = this.tokenClaims.id;
    });
    this.taskService.tasks$.subscribe((tasks) => {
      this.tasks = tasks;
      this.userT = this.tasks.filter((t) => t.user_id == this.userId);
    });
    this.taskService.refreshTasks();
  }

  ngOnInit(): void {

  }
}
