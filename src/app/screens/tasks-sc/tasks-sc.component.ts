import { Component, OnInit ,Input} from '@angular/core';
import { Task } from 'src/app/models/Task';
import { User } from 'src/app/models/User';
import { Labels } from 'src/app/models/Labels';
//import { Dialog } from 'src/app/models/dialog';

import { TaskService } from 'src/app/services/task.service';
import { SaerchTasks } from 'src/app/models/SaerchTasks';
import { DialogService, DynamicDialogRef } from 'primeng/dynamicdialog';
import { MessageService  } from 'primeng/api';
import { AddTaskComponent } from 'src/app/components/add-task/add-task.component';
import { Respon } from 'src/app/models/Respon';

import { AddTask } from 'src/app/models/AddTask';



@Component({
  selector: 'app-tasks-sc',
  templateUrl: './tasks-sc.component.html',
  styleUrls: ['./tasks-sc.component.css'],
  providers: [DialogService, MessageService],
})
export class TasksScComponent implements OnInit {
  tasks: Task[] = [];
  editT!: Task;
  addMode: boolean = true;
editMode: boolean = false;

  // dialog!: Dialog;
  constructor(
    private taskSvc: TaskService,
    private messageService: MessageService,
    public dialogService: DialogService
  ) {
    /* this.editT = {
      id: -1,
      name: '',
      description: '',
      due_date: new Date(),
      completed: false,
      user_id: -1,
      user_name: '',
      prod_date: new Date(),
      labels: [],
    };*/
  }

  show: boolean = false;
  Show() {
   this.show = this.show == false ? true : false;
  }

  edit(task: Task) {
    this.editT = task;
    this.Show();
  }

  /* ref!: DynamicDialogRef | undefined;
  showDialog() {
    this.ref = this.dialogService.open(AddTaskComponent, {
      header: 'הוסף משימה',
      showHeader: true,
      width: '70%',
      contentStyle: { overflow: 'auto' },
      baseZIndex: 10000,


    });
    this.ref.onClose.subscribe((result: any) => {
      if (result && result.maximized) {
        this.messageService.add({
          severity: 'info',
          summary: 'Maximized',
          detail: 'Dialog is maximized.',
        });
      }
    });
  }
  ngOnDestroy() {
    if (this.ref) {
      this.ref.close();
    }
  }*/

  ngOnInit(): void {
    this.taskSvc.GetTasks().subscribe((data) => (this.tasks = data)); //tasks

    //this.labelService.GetLabelList().subscribe((data) => (this.labelList = data));
    this.editT = {
      id: -1,
      name: '',
      description: '',
      due_date: new Date(),
      completed: false,
      user_id: -1,
      user_name: '',
      prod_date: new Date(),
      labels: [],
    };
  }
}
