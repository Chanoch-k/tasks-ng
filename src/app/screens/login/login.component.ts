import { HttpHeaders, HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from 'src/app/services/auth.service';
import { AuthInfo } from '../../models/Auth/AuthInfo';
import { LoginResponse } from '../../models/Auth/LoginResponse';
import {  MessageService } from 'primeng/api';
import { Subscription } from 'rxjs';
import { AuthError } from '../../models/messages/authError';
import { Welcome } from '../../models/messages/welcome';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css'],
})
export class LoginComponent implements OnInit {
  authInfo: AuthInfo = new AuthInfo();
  savedResponse: LoginResponse = new LoginResponse();
  constructor(
    private http: HttpClient,
    private router: Router,
    private authSvc: AuthService,
    private messageService: MessageService,
  ) {
    this.savedResponse.roll = -1;
  }

  ngOnInit(): void {
  this.messageService.add(new Welcome());

  }

  checkUser() {
    this.authSvc.GenerateToken(this.authInfo).subscribe((response) => {
      this.savedResponse = response; //save response when coming
      if (this.savedResponse.bearerString == '403') {
        this.messageService.add(new AuthError());
      }
       else {
        if (this.savedResponse.bearerString != null)
       sessionStorage.setItem('bearer', this.savedResponse.bearerString);
       if (this.savedResponse.name != null)
          sessionStorage.setItem('name', this.savedResponse.name);
        if (this.savedResponse.roll != null)
          sessionStorage.setItem('roll', this.savedResponse.roll.toString());
          switch (this.savedResponse.roll) {
            case 1: {
              this.router.navigate(['/users']);
              break;
            }
            case 2: {
              this.router.navigate(['/app']);
              break;
            }
            case 3: {
              this.router.navigate(['/app']);
              break;
            }
            case 4: {
              this.router.navigate(['/app']);
              break;
            }
            case 5: {
              this.router.navigate(['/app']);
              break;
            }
            default: {
              alert('role code error:' + this.savedResponse.roll);
              break;
            }
          }
      }
    });
  }
}
