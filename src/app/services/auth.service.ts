import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { catchError, Observable, retry, throwError } from 'rxjs';
import { AuthInfo } from '../models/Auth/AuthInfo';
import { LoginResponse } from '../models/Auth/LoginResponse';

@Injectable({
  providedIn: 'root',
})
export class AuthService {
  constructor(
    private httpClient: HttpClient,
  ) {}
  GenerateTokenURL: string = 'https://localhost:44340/api/Auth/GenerateToken';
  GetTokenClaimsURL: string = 'https://localhost:44340/api/Auth/GetTokenClaims';
  AmISuperUserURL: string = 'https://localhost:44340/api/Auth/AmISuperUser';

  GenerateToken(authInfo: AuthInfo): Observable<LoginResponse> {
    let httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json;charset=utf-8',
      }),
    };
    return this.httpClient.post<LoginResponse>(
      this.GenerateTokenURL,
      authInfo,
      httpOptions
    );
  }

  GetTokenClaims(): Observable<number> {
    let httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json;charset=utf-8',
        Authorization: this.getBearer(),
      }),
    };
    return this.httpClient.post<number>(
      this.GetTokenClaimsURL,
      null,
      httpOptions
    );
  }
  getBearer() {
    let tmp: string | null = sessionStorage.getItem('bearer');
    let autorization = '';
    if (tmp != null) {
      autorization = tmp;
    }
    return autorization;
  }
}
