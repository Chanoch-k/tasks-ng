import { Injectable } from '@angular/core';
import { User } from '../models/User';
import { AddUser } from '../models/AddUser';

import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable, Subject, BehaviorSubject } from 'rxjs';
import { tap } from 'rxjs/operators';
import { AuthService } from './auth.service';
import { DatePipe } from '@angular/common';
import { Respon } from '../models/Respon';

@Injectable({
  providedIn: 'root',
})
export class UsersService {
  users: User[] = [];
  SUserURL: string = 'https://localhost:44340/api/Users/SUser';
  UUserURL: string = 'https://localhost:44340/api/Users/UUser';
  AUserURL: string = 'https://localhost:44340/api/Users/AUser';
  GetUsersURL: string = 'https://localhost:44340/api/Users/GetUsers';

  GetUsers(): Observable<User[]> {
    let httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json;charset=utf-8',
        Authorization: this.authSvc.getBearer(),
      }),
    };
    return this.http.post<User[]>(this.GetUsersURL,'', httpOptions);
  }
  /*כדי לרענן רשימה בהוספה/מחיקה/עריכה */
  private usersSubject = new BehaviorSubject<User[]>([]);
  users$ = this.usersSubject.asObservable();
  private fetchUsers() {
     let httpOptions = {
       headers: new HttpHeaders({
         'Content-Type': 'application/json;charset=utf-8',
         Authorization: this.authSvc.getBearer(),
       }),
     };
    this.http.post<User[]>(this.GetUsersURL, '', httpOptions).subscribe(
      (users) => this.usersSubject.next(users),
      (error) => console.error('Error fetching tasks', error)
    );
  }
  refreshUsers() {
    this.fetchUsers();
  }
  AUser(user: AddUser) {
    let httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json;charset=utf-8',
        Authorization: this.authSvc.getBearer(),
      }),
    };
    return this.http
      .post<Respon>(this.AUserURL, user, httpOptions)
      .pipe(tap(() => this.fetchUsers()));
  }
  SUser() {}
  UUser() {}

  constructor(private http: HttpClient, private authSvc: AuthService) {}
}
