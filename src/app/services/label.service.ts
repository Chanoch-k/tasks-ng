import { Injectable } from '@angular/core';
import { Labels } from '../models/Labels';
import { LabelList } from '../models/LabelList';
import { Observable, Subject, BehaviorSubject } from 'rxjs';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { AuthService } from './auth.service';
import { Respon } from '../models/Respon';
import { filter, tap } from 'rxjs/operators';
@Injectable({
  providedIn: 'root',
})
export class LabelService {
  GetLabelListURL: string = 'https://localhost:44340/api/Labels/GetLabelList';
  DLabelURL: string = 'https://localhost:44340/api/Labels/DLabel'; //delete from label list
  ALabelURL: string = 'https://localhost:44340/api/Labels/ALabel'; //delete from label list
  ULabelURL: string = 'https://localhost:44340/api/Labels/ULabel'; //delete from label list

  KeyName: string = 'LabelList';
  labelList: LabelList[] = [];
  labels: Labels[] = [];

  GetLabelList(): Observable<LabelList[]> {
    let httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json;charset=utf-8',
        Authorization: this.authSvc.getBearer(),
      }),
    };
    return this.http.post<LabelList[]>(this.GetLabelListURL, '', httpOptions);
  }
  /*כדי לרענן רשימה בהוספה/מחיקה/עריכה */
  private labelsSubject = new BehaviorSubject<LabelList[]>([]);
  labels$ = this.labelsSubject.asObservable();
  private fetchLabels() {
    let httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json;charset=utf-8',
        Authorization: this.authSvc.getBearer(),
      }),
    };
    this.http
      .post<LabelList[]>(this.GetLabelListURL, '', httpOptions)
      .subscribe(
        (labels) => this.labelsSubject.next(labels),
        (error) => console.error('Error fetching labels', error)
      );
  }
  refreshLabels() {
    this.fetchLabels();
  }
  DLabel(id: number): Observable<Respon> {
    this.DLabelURL =
      'https://localhost:44340/api/Labels/DLabel/' + id.toString();
    let httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json;charset=utf-8',
        Authorization: this.authSvc.getBearer(),
      }),
    };
    return this.http
      .delete<Respon>(this.DLabelURL, httpOptions)
      .pipe(tap(() => this.fetchLabels()));
  }
  ALabel(label: LabelList) {
    let httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json;charset=utf-8',
        Authorization: this.authSvc.getBearer(),
      }),
    };
    return this.http
      .post<Respon>(this.ALabelURL, label, httpOptions)
      .pipe(tap(() => this.fetchLabels()));
  }
  ULabel(label: LabelList): Observable<Respon> {
    let httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json;charset=utf-8',
        Authorization: this.authSvc.getBearer(),
      }),
    };
    return this.http
      .post<Respon>(this.ULabelURL, label, httpOptions)
      .pipe(tap(() => this.fetchLabels()));;
  }
  constructor(private http: HttpClient, private authSvc: AuthService) {}
  getLabels() {
    return this.labels;
  }
}




