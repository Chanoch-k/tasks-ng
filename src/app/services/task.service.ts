import { Injectable, EventEmitter } from '@angular/core';
import { Task } from '../models/Task';
import { TasksWithLabels } from '../models/TasksWithLabels';
import { AddTask } from '../models/AddTask';
import { Respon } from '../models/Respon';
import { SaerchTasks } from '../models/SaerchTasks';
import { LabelService } from './label.service';
import { Labels } from '../models/Labels';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable, Subject, BehaviorSubject } from 'rxjs';
import { filter, tap } from 'rxjs/operators';
import { AuthService } from './auth.service';
import { DatePipe } from '@angular/common';
import { LabelList } from '../models/LabelList';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root',
})
export class TaskService {
  // KeyName: string = 'TaskList';
  //  tasks: Task[] = [];
  labelList: LabelList[] = [];
  labels: Labels[] = [];

  tasksWithLabels: TasksWithLabels[] = [];

  STasksURL: string = 'https://localhost:44340/api/Tasks/STasks';
  UTasksURL: string = 'https://localhost:44340/api/Tasks/UTasks';
  ATasksURL: string = 'https://localhost:44340/api/Tasks/ATasks';
  GetTasksURL: string = 'https://localhost:44340/api/Tasks/GetTasks';
  DTasksURL: string = 'https://localhost:44340/api/Tasks/DTask';
  constructor(
    private http: HttpClient,
    private authSvc: AuthService,
    public datepipe: DatePipe,
    private labelService: LabelService
  ) {
    this.labels = labelService.getLabels();
  }

  GetTasks(): Observable<Task[]> {
    let httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json;charset=utf-8',
        Authorization: this.authSvc.getBearer(),
      }),
    };
    return this.http.post<Task[]>(this.GetTasksURL, '', httpOptions);
  }
  /*כדי לרענן רשימה בהוספה/מחיקה/עריכה */
  private tasksSubject = new BehaviorSubject<Task[]>([]);
  tasks$ = this.tasksSubject.asObservable();
  private fetchTasks() {
    let httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json;charset=utf-8',
        Authorization: this.authSvc.getBearer(),
      }),
    };
    this.http.post<Task[]>(this.GetTasksURL, '', httpOptions).subscribe(
      (tasks) => this.tasksSubject.next(tasks),
      (error) => console.error('Error fetching tasks', error)
    );
  }
  refreshTasks() {
    this.fetchTasks();
  }
  /*
  GetUserTask(id: number): Observable<Task[]> {
    let httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json;charset=utf-8',
        Authorization: this.authSvc.getBearer(),
      }),
    };
    return this.http
      .post<Task[]>(this.GetTasksURL, '', httpOptions)
      .pipe(map((tasks) => tasks.filter((task) => task.id === id)));
  }

  */

  ATask(task: AddTask) {
    let httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json;charset=utf-8',
        Authorization: this.authSvc.getBearer(),
      }),
    };
    return this.http
      .post<Respon>(this.ATasksURL, task, httpOptions)
      .pipe(tap(() => this.fetchTasks()));

  }

  DTask(id: number): Observable<Respon> {
    this.DTasksURL = 'https://localhost:44340/api/Tasks/DTask/' + id.toString();
    let httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json;charset=utf-8',
        Authorization: this.authSvc.getBearer(),
      }),
    };
    return this.http
      .delete<Respon>(this.DTasksURL, httpOptions)
      .pipe(tap(() => this.fetchTasks()));
  }
  UTask(task: Task): Observable<Respon> {
    let httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json;charset=utf-8',
        Authorization: this.authSvc.getBearer(),
      }),
    };
    return this.http
      .post<Respon>(this.UTasksURL, task, httpOptions)
      .pipe(tap(() => this.fetchTasks()));;
  }
  STasks(
    saerchTasks: SaerchTasks,
    state: boolean = false,
    start: Date | null = null,
    end: Date | null = null
  ): Observable<Task[]> {
    let httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json;charset=utf-8',
        Authorization: this.authSvc.getBearer(),
        State: String(state),
      }),
    };
    let copyURl = this.STasksURL;
    if (start != null) {
      let strStart = this.datepipe.transform(start, 'yyyy-MM-dd');
      let strEnd = this.datepipe.transform(end, 'yyyy-MM-dd');
      copyURl += '/' + strStart + '/' + strEnd;
    } else {
      copyURl += '/None/None';
    }
    return this.http.post<Task[]>(copyURl, SaerchTasks, httpOptions);
  }
}
